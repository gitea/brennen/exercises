#!/usr/bin/perl 

=head1 NAME

1.pl

=head1 SYNOPSIS

perl ./1.pl

=head1 DESCRIPTION

Project Euler, Problem 1:

If we list all the natural numbers below 10 that are multiples of 3 or 5, we
get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

=head1 AUTHOR

Brennen Bearnes <bbearnes@gmail.com>
http://p1k3.com/

=cut

use strict;
use warnings;
use 5.10.0;

my $sum = 0;
for (1..999) {
  my $yesh = ((!($_ % 3)) || (!($_ % 5)));
  $sum += $_ if $yesh;
}
say $sum;
