#!/usr/bin/perl 

=pod

=head1 NAME

2.pl

=head1 SYNOPSIS

perl ./2.pl 

=head1 DESCRIPTION

Project Euler, Problem 2:

Each new term in the Fibonacci sequence is generated by adding the previous two
terms. By starting with 1 and 2, the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

Find the sum of all the even-valued terms in the sequence which do not exceed
four million.

=head1 AUTHOR

Brennen Bearnes <bbearnes@gmail.com>
http://p1k3.com/

=cut

use strict;
use warnings;
use 5.10.0;

my ($sum, $prev, $cur) = (0, 0, 1);
while ($cur < 4e6) {
  $sum += $cur if $cur =~ /[02468]$/;
  my $tmp = $cur;
  $cur += $prev;
  $prev = $tmp;
}
say $sum;
